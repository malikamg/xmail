package com.malik.util;

import com.malik.entities.Company;

public class MailStatu {

	private Company company;
	private int sendResult;
	private String colorResult;

	public MailStatu() {
		this.company=null;
		sendResult=0;
		colorResult="btn btn-default";

	}
	

	public MailStatu(Company company, int sendResult ) {
	
		this.company = company;
		this.sendResult = sendResult;
		if(sendResult==0){
			colorResult="btn btn-default";
		}
		else if(sendResult==1){
			colorResult="btn btn-success";
		}
		else{
			colorResult="btn btn-danger";
		}
	}


	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public int getSendResult() {
		return sendResult;
	}

	public void setSendResult(int sendResult) {
		this.sendResult = sendResult;
	}

	public String getColorResult() {
		return colorResult;
	}

	public void setColorResult(String colorResult) {
		this.colorResult = colorResult;
	}


	@Override
	public String toString() {
		return "MailStatu [company=" + company + ", sendResult=" + sendResult
				+ ", colorResult=" + colorResult + "]";
	}
	

}
