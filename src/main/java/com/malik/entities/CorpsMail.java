package com.malik.entities;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class CorpsMail {
	
	private int id;
	private String  objet;
	private String corp;
	private String langue;


	
public CorpsMail() {

}
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	 @Column(columnDefinition = "TEXT")
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	 @Column(columnDefinition = "TEXT")
	public String getCorp() {
		return corp;
	}
	public void setCorp(String corp) {
		this.corp = corp;
	}
	 @Column(columnDefinition = "TEXT")
	public String getLangue() {
		return langue;
	}
	public void setLangue(String langue) {
		this.langue = langue;
	}
	@Override
	public String toString() {
		return "CorpsMail [id=" + id + ", objet=" + objet + ", corp=" + corp
				+ "]";
	}
	
	

	
	

}
