package com.malik.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Lettre {
	
	private int id;
	private String domaine;
	private String contenu ;
	private String path;
	private String langue;
	

	public Lettre() {
		
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	 @Column(columnDefinition = "TEXT")
	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getLangue() {
		return langue;
	}
	public void setLangue(String langue) {
		this.langue = langue;
	}
	@Override
	public String toString() {
		return "Lettre [id=" + id + ", domaine=" + domaine + ", contenu="
				+ contenu + ", path=" + path + ", langue=" + langue + "]";
	}
	
	
	

}
