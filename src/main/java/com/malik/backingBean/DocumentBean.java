package com.malik.backingBean;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.malik.entities.Cv;
import com.malik.entities.Lettre;
import com.malik.persistence.DocumentPersistence;
import com.malik.service.Pdf;

@ManagedBean

public class DocumentBean {
	
	DocumentPersistence dp;
	
	private Cv cv;
	private Lettre lettre;
	private Lettre selectedLettre;
	private String domaineLettre="java";
	private String langueLettre="fr";
	private String contenuLettre;
	private Pdf pdf;
	
	
	public DocumentBean() {
		// TODO Auto-generated constructor stub
		
	}
	@PostConstruct
	private void init(){
		dp=new DocumentPersistence();
		cv=new Cv();
		lettre=new Lettre();
		
	}
	


	public void doSaveLettre(){
		lettre=new Lettre();
		lettre.setContenu(contenuLettre);
		lettre.setDomaine(domaineLettre);
		lettre.setLangue(langueLettre);
		lettre.setPath("");
		
		
		dp.persist(lettre);

	}
	public ArrayList<Lettre>showAllLettre(){
		
		return (ArrayList<Lettre>) dp.findAll();
	}
	
	
    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        results.add("java/jee");
        results.add("bigdata");
        results.add("bi");

         
        return results;
    }
    
	public void doDelete(){
		dp.remove(selectedLettre);
	}
    
	public String doPrint(){
		return lettre.toString();
	}
	
	public Cv getCv() {
		return cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}

	public Lettre getLettre() {
		return lettre;
	}

	public void setLettre(Lettre lettre) {
		this.lettre = lettre;
	}

	public String getDomaineLettre() {
		return domaineLettre;
	}

	public void setDomaineLettre(String domaineLettre) {
		this.domaineLettre = domaineLettre;
	}

	public String getLangueLettre() {
		return langueLettre;
	}

	public void setLangueLettre(String langueLettre) {
		this.langueLettre = langueLettre;
	}
	public String getContenuLettre() {
		return contenuLettre;
	}
	public void setContenuLettre(String contenuLettre) {
		this.contenuLettre = contenuLettre;
	}
	public Lettre getSelectedLettre() {
		return selectedLettre;
	}
	public void setSelectedLettre(Lettre selectedLettre) {
		this.selectedLettre = selectedLettre;
	}

}
