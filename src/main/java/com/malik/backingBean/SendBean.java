package com.malik.backingBean;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.malik.entities.Company;
import com.malik.entities.CorpsMail;
import com.malik.entities.Cv;
import com.malik.entities.Lettre;
import com.malik.entities.User;
import com.malik.persistence.CompanyPersistence;
import com.malik.persistence.CorpMailPersistence;
import com.malik.persistence.CvPersistence;
import com.malik.persistence.DocumentPersistence;
import com.malik.service.PathDir;
import com.malik.service.Pdf;
import com.malik.service.SendEmail;
import com.malik.util.MailStatu;

@ManagedBean
@ViewScoped
public class SendBean implements Serializable {
	@ManagedProperty("#{userBean.fetchedUser}")
	private User user;

	private CompanyPersistence cp;
	private Company company;

	private SendEmail sendMail;
	private boolean selected;

	private String objet;
	private String corpMail;
	private String message;

	private ArrayList<Company> companys;
	private List<Company> selectedCompany;

	private CvPersistence cvp;
	private DocumentPersistence dp;
	private CorpMailPersistence cmp;

	private List<Lettre> lettreListe;
	private List<Cv> cvListe;
	private List<CorpsMail> corpMailList;
	
	private ArrayList<MailStatu>mailStatuList;
	private MailStatu mailStatus;
	
	private boolean afficher;
	
	private int progress;  
	



	@PostConstruct
	private void init() {

		cp = new CompanyPersistence();
		cvp = new CvPersistence();
		dp = new DocumentPersistence();
		cmp = new CorpMailPersistence();

		this.company = new Company();
		companys = (ArrayList<Company>) cp.findAll();
		sendMail = new SendEmail();

		lettreListe = dp.findAll();
		cvListe = cvp.findAll();
		corpMailList = cmp.findAll();
		mailStatuList=new ArrayList<MailStatu>();
		afficher=true;
		
	}
	
	public ArrayList<MailStatu>showMailStatu(){
		return this.mailStatuList;
	}

	public ArrayList<Company> getAll() {
		companys = (ArrayList<Company>) cp.findAll();
		return companys;
	}

	public void onRowSelect() {
		//System.out.println("add");
		//System.out.println(selectedCompany);

	}

	public void doSaveSelected() {
		// System.out.println(selectedCompany);
	}

	public String resultClass(int result) {
		String cssClass = "btn btn-danger";
		if (result == 1) {
			cssClass = "btn btn-success";
		}
		else if(result==0){
			cssClass ="btn btn-default";
		}
		return cssClass;
	}
	
	public boolean show(){
		if(afficher==false)
		afficher=true;
		else afficher=false;
		//System.out.println(afficher);
		return afficher;
	}
	
    public void startTestFunction() {
       // System.out.println("Staring Test Function");

        for (int i = 0; i < 100; i++) {
            setProgress(i);
        	
          //  System.out.println("TestFunction - setting progress to: " + i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
        setProgress(100);
       // System.out.println("Finished Function");
    }
    
    public int  getProgress() {  
        return progress;  
    }  


    public void setProgress(int progress) { 
        this.progress =progress;  
    }  

    public void onComplete() {  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Progress Completed", "Progress Completed"));  
    }  

	/**
	 * 
	 * most important function
	 * 
	 * @throws IOException
	 */
	public void doSend() throws IOException {
		List<String> file = new ArrayList<String>();
		CorpsMail corpMail = new CorpsMail();
		Lettre letterMotivation=null;
		
		int statu=0;


		if (selectedCompany.size() == 0 ||selectedCompany==null) {
			message = "il faut selectionné une ou plusieur entreprise";
			FacesMessage msg =  new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", message);
            FacesContext.getCurrentInstance().addMessage(null, msg);
			//System.out.println(message);

		} else {
		int i=0;
			for (Company c : selectedCompany) {
				setProgress(i+=(100/selectedCompany.size()));
		
				if (c.getLangue().equals("fr")) {
					corpMail = corpMailList.get(0);
					letterMotivation = generateLetter(c);
					file.add(cvListe.get(0).getPath());
					file.add(letterMotivation.getPath());

				} else if (c.getLangue().equals("an")) {
					corpMail = corpMailList.get(1);
					letterMotivation = generateLetter(c);
					file.add(cvListe.get(1).getPath());
					file.add(letterMotivation.getPath());
				}
				//System.out.println(file);
				String mailBody=corpMail.getCorp().replace("####", c.getNom()).replace(".",". <br/>").replace("..", ". <br/><br/>").replace("?", "&#39;");
				statu=sendMail.send(c.getEmail(), corpMail.getObjet(),mailBody, file, user.getEmail(),user.getPassword());
	
				file=new ArrayList<String>();
				mailStatuList.add(new MailStatu(c,statu));
				//System.out.println(mailStatuList);

			}// endfor
			 show();
			 setProgress(100);
		}//end else
	
	}// endSend

	
	private Lettre generateLetter(Company company) throws IOException {
		Lettre generatedLetter = null;
		Pdf pdf = new Pdf();
		for (Lettre l : lettreListe) {
			if ((l.getLangue().equals(company.getLangue()))
					&& (l.getDomaine().equals(company.getDomaine()))) {
				generatedLetter = l;
				//System.out.println("test: "+generatedLetter);
				break;
			}
		}
	
			String path = "C:/crossMail/lettres/" + company.getDomaine() + "/"+ company.getLangue() + "/" + company.getNom()+"/";
			PathDir.createPath(path+"lettre_de_motivation.pdf");
			String contenu = generatedLetter.getContenu().replace("####"," " + company.getNom() + " ");
			generatedLetter.setPath(path + "lettre_de_motivation.pdf");
			pdf.generatePDF(contenu, path+ "lettre_de_motivation.pdf");
			dp.persist(generatedLetter);//to delete this ligne
		
		return generatedLetter;
	}

	/*public  ArrayList<MailStatu>showMailStatu(){
		return mailStatusList;
	}*/
	// ///////////////////////////////setter &
	// getter//////////////////////////////////////
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Company> getCompanys() {
		return companys;
	}

	public void setCompanys(ArrayList<Company> companys) {
		this.companys = companys;
	}

	public List<Company> getSelectedCompany() {
		return selectedCompany;
	}

	public void setSelectedCompany(ArrayList<Company> selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getCorpMail() {
		return corpMail;
	}

	public void setCorpMail(String corpMail) {
		this.corpMail = corpMail;
	}


	public ArrayList<MailStatu> getMailStatuList() {
		return mailStatuList;
	}

	public void setMailStatuList(ArrayList<MailStatu> mailStatuList) {
		this.mailStatuList = mailStatuList;
	}

	public MailStatu getMailStatus() {
		return mailStatus;
	}

	public void setMailStatus(MailStatu mailStatus) {
		this.mailStatus = mailStatus;
	}

	public boolean isAfficher() {
		return afficher;
	}

	public void setAfficher(boolean afficher) {
		this.afficher = afficher;
	}


}//classEND