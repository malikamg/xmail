package com.malik.backingBean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.malik.entities.CorpsMail;
import com.malik.persistence.CorpMailPersistence;
@ManagedBean
@ViewScoped
public class CorpMailBean {
	
	private CorpMailPersistence cmp;
	private CorpsMail corpMail;
	private CorpsMail selectedCorpMail;
	
	private String objetMail;
	private String conteuMail;
	private String langue="fr";
	
	@PostConstruct
	private void init(){
		cmp=new CorpMailPersistence();
		corpMail=new CorpsMail();
	}
	public void doSave(){
		corpMail=new CorpsMail();
		if(langue.equals("fr")){
			corpMail.setId(1);
		}else{
			corpMail.setId(2);
			
		}
		corpMail.setCorp(conteuMail);
		corpMail.setObjet(objetMail);
		corpMail.setLangue(langue);
		cmp.persist(corpMail);
	}
	
	public List<CorpsMail>show(){
		return cmp.findAll();
	}

	public void doDelete(){
		cmp.remove(corpMail);
	}
	////////////////////////getter & setter ////////////////////////////////
	public CorpsMail getCorpMail() {
		return corpMail;
	}

	public void setCorpMail(CorpsMail corpMail) {
		this.corpMail = corpMail;
	}

	public String getObjetMail() {
		return objetMail;
	}

	public void setObjetMail(String objetMail) {
		this.objetMail = objetMail;
	}

	public String getLangue() {
		return langue;
	}

	public void setLangue(String langue) {
		this.langue = langue;
	}
	public String getConteuMail() {
		return conteuMail;
	}
	public void setConteuMail(String conteuMail) {
		this.conteuMail = conteuMail;
	}
	public CorpsMail getSelectedCorpMail() {
		return selectedCorpMail;
	}
	public void setSelectedCorpMail(CorpsMail selectedCorpMail) {
		this.selectedCorpMail = selectedCorpMail;
	}
	

}
