package com.malik.backingBean;

import java.sql.Date;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.malik.entities.User;
import com.malik.persistence.UserPersistence;

@ManagedBean
@ApplicationScoped
public class UserBean {
	private String prenom;
	private String nom;
	private String adresse;
	private String telephone;
	private String poste;
	private String dateNaissance;
	private String email;
	private String password;
	private String message;
	private boolean render;

	private User user;
	private User fetchedUser;
	private ArrayList<User> users;
	private UserPersistence up;

	public UserBean() {

	}

	@PostConstruct
	private void init() {
		up = new UserPersistence();
		fetchedUser = showOneUser();

		render = true;

		if (fetchedUser == null) {
			message = "il faut cree un utilisateur";
			render = false;
/*
			prenom = "prenom";
			nom = "nom";
			adresse = "adresse";
			telephone = "telephone";
			poste = "poste ou année d etude";
			dateNaissance = "date de naissance";
			email = "email";
			password = "password";
			*/

		} else {

			message = "Bienvenu ," + fetchedUser.getNom()
					+ "  vous pouvez modifier vos info ici sinon tout est OK";

			prenom = fetchedUser.getPrenom();
			nom = fetchedUser.getNom();
			adresse = fetchedUser.getAdresse();
			telephone = fetchedUser.getTelephone();
			poste = fetchedUser.getPoste();
			dateNaissance = fetchedUser.getDateNaissance();
			email = fetchedUser.getEmail();
			password = fetchedUser.getPassword();

		}
	}

	public void doSave() {
		user = new User();
		if (user != null) {
			
			user.setAdresse(adresse);
			user.setDateNaissance(dateNaissance);
			user.setEmail(email);
			user.setNom(nom);
			user.setPrenom(prenom);
			user.setPassword(password);
			user.setPoste(poste);
			user.setTelephone(telephone);
			//System.out.println(user);
			// if (fetchedUser != null)
			user.setId(1);
			up.persist(user);

            FacesMessage msg = new FacesMessage("Succesful", user.getPrenom() + " est enregistré");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        else{
        	FacesMessage msg =  new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "information manquante");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
	}

	public void doCancel() {
		user = new User();
		//System.out.println("do cancel");
	}

	public ArrayList<User> show() {
		users = (ArrayList<User>) up.findAll();
		return users;
	}

	public User showOneUser() {
		if (show().size() > 0) {
			return show().get(0);
		} else {
			return null;
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getRender() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public User getFetchedUser() {
		return fetchedUser;
	}

	public void setFetchedUser(User fetchedUser) {
		this.fetchedUser = fetchedUser;
	}

}
