package com.malik.backingBean;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;



import javax.faces.context.FacesContext;

import com.malik.entities.Company;
import com.malik.entities.User;
import com.malik.persistence.UserPersistence;

@ManagedBean
@ViewScoped
public class HomeBean {
	
	private User user;
	private Company company;
	UserPersistence up=new UserPersistence();
	
	public HomeBean() {
	}
	@PostConstruct
	private void  init(){
		user=new User();
		user=up.find(1);
		
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	public void redirection() throws IOException {
		 FacesContext.getCurrentInstance().getExternalContext().redirect("page/home/index.xhtml");
	}
	
	

}
