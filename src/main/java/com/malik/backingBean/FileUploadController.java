package com.malik.backingBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.primefaces.model.UploadedFile;

import com.malik.entities.Cv;
import com.malik.persistence.CvPersistence;
import com.malik.service.PathDir;
@ManagedBean
@ViewScoped
public class FileUploadController {
	private  CvPersistence cvp;
    private UploadedFile file;
    private String cvLangue="fr";
    private Cv cv;

    @PostConstruct
    private void init(){
    	System.out.println("init file upload");
    	cvp=new CvPersistence();
    }
    


    public void upload()   {
    	 System.out.println("upload start");
        if(file != null  && !(file.getFileName().equals(""))) {
        	
        	cv=new Cv();
        
        	String path="C:/crossMail/cv/";
        	
        	if(cvLangue.equals("fr")){
        		cv.setId(1);
        	}else{
        		cv.setId(2);    		
        	}
    		cv.setLangue(cvLangue);
    		cv.setTitre(file.getFileName());
    		path+=cvLangue+"/";
    		
    		try {
				PathDir.createPath(path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		cv.setPath(path+file.getFileName());
    		System.out.println("cv upload");
    		fileUpload(cv.getPath());
    		cvp.persist(cv);
            FacesMessage msg = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        else{
        	FacesMessage msg =  new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "il y a pas de CV");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
	

	 

//fileuplode methode
public void fileUpload(String path) {  

    //get uploaded file from the event
    UploadedFile uploadedFile = file;
    //create an InputStream from the uploaded file
    InputStream inputStr = null;
    try {
        inputStr = uploadedFile.getInputstream();
    } catch (IOException e) {
        //log error
    }

    //create destination File
    String destPath = path;
    File destFile = new File(destPath);

    //use org.apache.commons.io.FileUtils to copy the File
    try {                    
        FileUtils.copyInputStreamToFile(inputStr, destFile);
    } catch (IOException e) {
        //log error
    }
}


	 
	 //////////////////////////////getter & setter/ /////////////////////////////////////////

  
	public ArrayList<Cv> show() {
		ArrayList<Cv>cvs = (ArrayList<Cv>) cvp.findAll();
		return cvs;
	}

	public String getCvLangue() {
		return cvLangue;
	}

	public void setCvLangue(String cvLangue) {
		this.cvLangue = cvLangue;
	}

	public Cv getCv() {
		return cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}
	
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
}