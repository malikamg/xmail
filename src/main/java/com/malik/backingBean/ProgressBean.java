package com.malik.backingBean;
import java.io.Serializable;  
import javax.faces.application.FacesMessage;  
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped // Does the scope matter here?
public class ProgressBean implements Serializable {  

    public ProgressBean() {};

    private int progress=0;  


    public void startTestFunction() {
        for (int i = 0; i < 100; i++) {
            setProgress(i);
           // System.out.println("TestFunction - setting progress to: " + i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }
        setProgress(100);
      //  System.out.println("Finished Function");
    }
    
    public int  getProgress() {  
        return progress;  
    }  


    public void setProgress(int progress) { 
        this.progress =progress;  
    }  

    public void onComplete() {  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Progress Completed", "Progress Completed"));  
    }  


}  