package com.malik.backingBean;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.malik.entities.Company;
import com.malik.persistence.CompanyPersistence;

@ManagedBean
@ViewScoped
public class CompanyBean {
	
	private CompanyPersistence cp;
	private ArrayList<Company> companys;
	private Company company;
	private Company selectedCompany;
	
	private String nom;
	private String domain;
	private String email;
	private String adresse;
	private String tel;
	private String langue="fr";

	
	

	public CompanyBean() {


	}
	@PostConstruct
	private void init() {
		//System.out.println("init");
		this.company = new Company();
		cp = new CompanyPersistence();
		companys=(ArrayList<Company>) cp.findAll();
		selectedCompany=new Company();
	}

	public void doSave() {
	    company=new Company();
	    if(company!=null){
	    company.setAdresse(adresse);
	    company.setDomaine(domain);
	    company.setEmail(email);
	    company.setLangue(langue);
	    company.setNom(nom);
	    company.setTel(tel);
	  
		cp.persist(company);	
		
	    FacesMessage msg = new FacesMessage("Succesful", company.getNom()+ " est enregisté");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    else{
    	FacesMessage msg =  new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "information manquante");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }


	}

	public ArrayList<Company> getAll() {
		companys = (ArrayList<Company>) cp.findAll();
		return companys;
	}
	
	public void doDelete(){
		//System.out.println("dodelete0");
		cp.remove(selectedCompany);
		//System.out.println("dodelete");
	}
	
///////////////////////////getter & setter///////////////////////////////////
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getLangue() {
		return langue;
	}
	public void setLangue(String langue) {
		this.langue = langue;
	}
	public ArrayList<Company> getCompanys() {
		return companys;
	}
	public void setCompanys(ArrayList<Company> companys) {
		this.companys = companys;
	}
	public Company getSelectedCompany() {
		return selectedCompany;
	}
	public void setSelectedCompany(Company selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

}
