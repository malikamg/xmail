package com.malik.backingBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class TestBean {

	boolean render1;
	
	@PostConstruct
	private void init(){
		render1=false;
	}
	
	public boolean show(){
		if(render1==false)
		render1=true;
		else render1=false;
		//System.out.println(render1);
		return render1;
	}

	public boolean isRender1() {
		return render1;
	}

	public void setRender1(boolean render1) {
		this.render1 = render1;
	}
	
}
