package com.malik.main;

import java.util.ArrayList;
import java.util.List;

import com.malik.entities.Company;

import com.malik.entities.User;
import com.malik.persistence.CompanyPersistence;
import com.malik.persistence.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

public class PersistentOperator {

 //   EntityManagerFactory emf = Persistence.createEntityManagerFactory("crossMail");
  //  EntityManager em=emf.createEntityManager();

   EntityManager em=PersistenceManager.getInstance().getEm();
   EntityTransaction entityTransaction = em.getTransaction();

    public void insert() {

        //--------------Sample data --------------
        User citizen1 = new User();
        User citizen2 = new User();
        
        citizen1.setNom("malik");
        citizen1.setPrenom("malik");

        citizen2.setNom("bader");
        citizen2.setPrenom("bader");
        


        //--------------Sample data --------------

   
        

        try {
            entityTransaction.begin();

            em.persist(citizen1);
            em.persist(citizen2);


            entityTransaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityTransaction.rollback();
        }
    }
    
    
    public List<User> findAll(){
    	List<User> allUser=null;
      
        EntityTransaction entityTransaction = em.getTransaction();
    	   try {
               entityTransaction.begin();

               allUser=  em.createQuery("select u from User u").getResultList();

               entityTransaction.commit();
           } catch (Exception e) {
               e.printStackTrace();
               entityTransaction.rollback();
           }
    	
    	return allUser;
    	
    }

    public static void main(String[] args) {
        PersistentOperator po = new PersistentOperator();
        CompanyPersistence cp =new CompanyPersistence();
        Company company=new Company();
        
   
        
        company.setAdresse("adresse");
        company.setDomaine(".net");
        company.setEmail("malik.bad@gmail.com");
        company.setNom("malik corp");
        company.setLangue("fr");
        cp.persist(company);
        
      for (Company c :  cp.findAll()) {
    	  System.out.println(c);
	}
      System.out.println("***************************************************");
        po.insert();
        System.out.println("allusers: "+(ArrayList<User>)po.findAll());
        for (User u : po.findAll()){
        	System.out.println(u);
        }
  
        
       
    }
}
