package com.malik.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.malik.entities.User;

public class UserPersistence {


	 EntityManager em=PersistenceManager.getInstance().getEm();
	EntityTransaction entityTransaction = em.getTransaction();

	
	
	public void persist(User user) {

		try {
			entityTransaction.begin();
			em.merge(user);
			//System.out.println("user persist");

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

	public User find(int id) {
		User user = null;

		try {
			entityTransaction.begin();

			user = em.find(User.class, id);
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
		return user;
	}

	public List<User> findAll() {
		List<User> allUser = null;
	
		try {
			entityTransaction.begin();

			allUser = em.createQuery("select u from User u").getResultList();

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}

		return allUser;

	}
	
	public void remove(User user){
		
		try {
			entityTransaction.begin();
			em.remove(user);
			//System.out.println("user persist");

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

}
