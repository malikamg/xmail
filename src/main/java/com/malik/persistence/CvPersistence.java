package com.malik.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.malik.entities.Cv;
import com.malik.entities.User;

public class CvPersistence {


	 EntityManager em=PersistenceManager.getInstance().getEm();
	EntityTransaction entityTransaction = em.getTransaction();

	
	
	public void persist(Cv cv) {

		try {
			entityTransaction.begin();
			em.merge(cv);
			//System.out.println("Cv persist");

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

	public Cv find(int id) {
		Cv cv = null;

		try {
			entityTransaction.begin();

			cv = em.find(Cv.class, id);
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
		return cv;
	}

	public List<Cv> findAll() {
		List<Cv> allCv = null;
	
		try {
			entityTransaction.begin();

			allCv = em.createQuery("select c from Cv c").getResultList();

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}

		return allCv;

	}
	
	public void remove(Cv cv){
		
		try {
			entityTransaction.begin();
			em.remove(cv);
		

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

}
