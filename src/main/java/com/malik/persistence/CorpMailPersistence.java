package com.malik.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.malik.entities.CorpsMail;
import com.malik.entities.User;

public class CorpMailPersistence {


	 EntityManager em=PersistenceManager.getInstance().getEm();
	EntityTransaction entityTransaction = em.getTransaction();

	
	
	public void persist(CorpsMail corpMail) {

		try {
			entityTransaction.begin();
			em.merge(corpMail);
			//System.out.println("Cv persist");

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

	public CorpsMail find(int id) {
		CorpsMail  corpMail = null;

		try {
			entityTransaction.begin();

			corpMail = em.find(CorpsMail.class, id);
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
		return corpMail;
	}

	public List<CorpsMail> findAll() {
		List<CorpsMail> allCorpMail = null;
	
		try {
			entityTransaction.begin();

			allCorpMail = em.createQuery("select c from CorpsMail c").getResultList();

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}

		return allCorpMail;

	}
	
	public void remove(CorpsMail corpMail){
		
		try {
			entityTransaction.begin();
			em.remove(corpMail);
	

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

}
