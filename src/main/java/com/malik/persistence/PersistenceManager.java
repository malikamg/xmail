package com.malik.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class PersistenceManager {
	
	private static PersistenceManager instance;
	private  EntityManagerFactory emf; 
	private  EntityManager em ;
	
	private PersistenceManager(){
		emf= Persistence.createEntityManagerFactory("crossMail");
		em= emf.createEntityManager();
	}
	
	public static PersistenceManager getInstance(){
		if(instance==null){
			instance=new PersistenceManager();
		}
		return instance;
	}

	public EntityManager getEm() {
		return em;
	}


}
