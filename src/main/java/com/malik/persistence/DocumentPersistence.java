package com.malik.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.malik.entities.Lettre;
import com.malik.entities.User;

public class DocumentPersistence {
	

	 EntityManager em=PersistenceManager.getInstance().getEm();
	EntityTransaction entityTransaction = em.getTransaction();

	
	
	public void persist(Lettre lettre) {

		try {
			entityTransaction.begin();
			em.persist(lettre);
			//System.out.println("lettre persist");

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}

	public Lettre find(int id) {
		Lettre lettre = null;

		try {
			entityTransaction.begin();

			lettre = em.find(Lettre.class, id);
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
		return lettre;
	}

	public List<Lettre> findAll() {
		List<Lettre> allLettre = null;
	
		try {
			entityTransaction.begin();

			allLettre = em.createQuery("select l from Lettre l").getResultList();

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}

		return allLettre;

	}
	
	public void remove(Lettre lettre){
		
		try {
			entityTransaction.begin();
			em.remove(lettre);
			

			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityTransaction.rollback();
		}
	}


}
