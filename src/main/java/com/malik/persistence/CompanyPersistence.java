package com.malik.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.malik.entities.Company;
import com.malik.entities.User;



public class CompanyPersistence {
	
	 EntityManager em=PersistenceManager.getInstance().getEm();
		EntityTransaction entityTransaction = em.getTransaction();

		
		
		public void persist(Company company) {

			try {
				entityTransaction.begin();
				em.persist(company);

				entityTransaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
				entityTransaction.rollback();
			}
		}

		public Company find(int id) {
			Company company = null;

			try {
				entityTransaction.begin();

				company = em.find(Company.class, id);
				entityTransaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
				entityTransaction.rollback();
			}
			return company;
		}

		public List<Company> findAll() {
			List<Company> allCompany = null;
		
			try {
				entityTransaction.begin();

				allCompany = em.createQuery("select c from Company c").getResultList();

				entityTransaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
				entityTransaction.rollback();
			}

			return allCompany;

		}
		
		public void remove(Company company){
			
			try {
				entityTransaction.begin();
				em.remove(company);
			

				entityTransaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
				entityTransaction.rollback();
			}
		}

}
