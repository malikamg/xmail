package com.malik.service;



import java.io.File;
import java.util.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmail {

    public int send(String to, String subject, String msg, List<String> file, final String from, final String password) {
        int rep = 0;
        Multipart multipart = new MimeMultipart();
        try {
            Properties props = new Properties();
            props.setProperty("mail.transport.protocol", "smtp");
            props.setProperty("mail.host", "smtp.gmail.com");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            props.put("mail.debug", "true");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");
            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(from, password);
                }
            });

            session.setDebug(false);//affiche sur la console les message et les etape d envoie
            Transport transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from);
            
            String msgs=msg+"hello";
            MimeMessage message = new MimeMessage(session);
            message.setSender(addressFrom);
            message.setSubject(subject);
            message.setText(msgs);  
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //bodypart for mail corp
			BodyPart messageBodyPart=new MimeBodyPart();
			messageBodyPart.setContent(msg, "text/html");
			multipart.addBodyPart(messageBodyPart);
			

            //bodypart block for uploaded file cv & lettre de motivation
            BodyPart fileBodyPart1 = new MimeBodyPart();
            BodyPart fileBodyPart2 = new MimeBodyPart();
            //messageBodyPart.setText(msgs);
            
            //multipart.addBodyPart(messageBodyPart);


            if (file != null) {
            	DataSource source;
            	//for (String f : file){
            		//System.out.println("test join"+file.get(0));

                source = new FileDataSource(file.get(0));
                fileBodyPart1.setDataHandler(new DataHandler(source));
                String ext = (file.get(0).lastIndexOf('.') == -1) ? "" : file.get(0).substring(file.get(0).lastIndexOf('.') + 1);
                String name=(file.get(0).lastIndexOf('/') == -1)? "" : file.get(0).substring(file.get(0).lastIndexOf('/') + 1,file.get(0).lastIndexOf('.') + 1);
                fileBodyPart1.setFileName(name + ext);
                multipart.addBodyPart(fileBodyPart1);
                
                source = new FileDataSource(file.get(1));
                fileBodyPart2.setDataHandler(new DataHandler(source));
                 ext = (file.get(1).lastIndexOf('.') == -1) ? "" : file.get(1).substring(file.get(1).lastIndexOf('.') + 1);
                 name=(file.get(1).lastIndexOf('/') == -1)? "" : file.get(1).substring(file.get(1).lastIndexOf('/') + 1,file.get(1).lastIndexOf('.') + 1);
                fileBodyPart2.setFileName(name + ext);
                multipart.addBodyPart(fileBodyPart2);
                
            	//}
            }



            message.setContent(multipart);

            transport.connect();
            Transport.send(message);
            transport.close();
            rep = 1;
        } catch (NoSuchProviderException ex) {
            System.err.println("pas de provid'eur'");
            rep += 10;
        } catch (AddressException ex) {
            System.err.println("addrexc : " + ex);
        } catch (MessagingException ex) {
            rep += 1100;
            System.err.println("msgexc : " + ex);
        }
        return rep;
    }
}
