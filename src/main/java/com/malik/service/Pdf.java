package com.malik.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class Pdf {
	Document document = new Document();
	
	public void generatePDF(String contenu,String path){

	      try
	      {
	         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
	         document.open();
	         document.add(new Paragraph(contenu));
	         document.close();
	         writer.close();
	        // System.out.println("pdf ok");
	      } catch (DocumentException e)
	      {
	         e.printStackTrace();
	      } catch (FileNotFoundException e)
	      {
	         e.printStackTrace();
	      }	
	}
}
