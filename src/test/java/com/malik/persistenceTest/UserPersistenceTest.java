package com.malik.persistenceTest;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.malik.entities.User;
import com.malik.persistence.UserPersistence;

public class UserPersistenceTest {
	UserPersistence up = new UserPersistence();
	User user;

	@Before
	public void setUp() {
		user = new User();
		user.setNom("usertest");
		user.setPrenom("userTest");
		
	}
    @Test
	public void testPersist() {
		 up.persist(user);
	}
    
    @Test
    public void testFind(){
    	System.out.println(up.find(1));
    	
    }
    
    @Test
    public void testFindAll(){
    	System.out.println(up.findAll());
    	
    }
}
